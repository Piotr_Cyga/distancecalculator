public class DistanceCalculatorFactory {
    public static DistanceCalculator getCalculator(Unit unit){

        switch (unit) {
            case METER:
                return new MeterDistanceCalculator();
            case FEET:
                return new FeetDistanceCalculator();
            case NM:
                return new NMDistanceCalculator();
            default:
                throw new IllegalStateException("Unexpected value: " + unit);
        }
    }
}
