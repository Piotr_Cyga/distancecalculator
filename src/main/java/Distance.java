public class Distance {
    private double value;
    private Unit unit;

    public Distance(double value, Unit unit) {
        if (value < 0) {
            throw new IllegalArgumentException("Distance value can't be negative");
        }
        this.value = value;
        this.unit = unit;
    }

    public double getValue() {
        return value;
    }

    public Unit getUnit() {
        return unit;
    }
}
