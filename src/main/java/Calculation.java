import java.util.List;

public class Calculation {
    private double value1;
    private double value2;
    private Unit unit1;
    private Unit unit2;
    private String operation;
    private DistanceCalculator calculator;

    public Calculation() {
    }

    public Calculation(double value1, double value2, Unit unit1, Unit unit2, String operation, DistanceCalculator calculator) {
        this.value1 = value1;
        this.value2 = value2;
        this.unit1 = unit1;
        this.unit2 = unit2;
        this.operation = operation;
        this.calculator = calculator;
    }

    public void setValue1(double value1) {
        this.value1 = value1;
    }

    public void setValue2(double value2) {
        this.value2 = value2;
    }

    public void setUnit1(Unit unit1) {
        this.unit1 = unit1;
    }

    public void setUnit2(Unit unit2) {
        this.unit2 = unit2;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public void setCalculator(DistanceCalculator calculator) {
        this.calculator = calculator;
    }

    public double getValue1() {
        return value1;
    }

    public double getValue2() {
        return value2;
    }

    public Unit getUnit1() {
        return unit1;
    }

    public Unit getUnit2() {
        return unit2;
    }

    public String getOperation() {
        return operation;
    }

    public DistanceCalculator getCalculator() {
        return calculator;
    }

    public double perform(List<String> errorMessages) {

        try {
            switch (operation) {
                case "add":
                    return calculator.add(new Distance(this.value1, this.unit1), new Distance(this.value2, this.unit2));
                case "subtract":
                    return calculator.subtract(new Distance(this.value1, this.unit1), new Distance(this.value2, this.unit2));
                case "multiply":
                    return calculator.multiply(new Distance(this.value1, this.unit1), new Distance(this.value2, this.unit2));
                case "divide":
                    return calculator.divide(new Distance(this.value1, this.unit1), new Distance(this.value2, this.unit2));
                default:
                    errorMessages.add(String.format("Unsupported operation: %s", operation));
                    return 0;

            }
        } catch (IllegalArgumentException e) {
            errorMessages.add(e.getMessage());
            return 0;
        }
    }

    public String getUnit() {
        return calculator.getUnitValue();
    }
}
