import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public enum CalculatorService {
    INSTANCE;

    public Calculation createCalculation(Map<String, String[]> requestParameters, List<String> errorMessages, List<String> warningMessages) {
        errorMessages.addAll(validateParameters(requestParameters, warningMessages));
        Calculation calculation = new Calculation();
        if (!errorMessages.isEmpty()) {
            return calculation;
        }
        String operation = requestParameters.get("operation")[0];
        calculation.setOperation(operation);

        try {
            calculation.setValue1(Double.parseDouble(requestParameters.get("value1")[0]));
            calculation.setValue2(Double.parseDouble(requestParameters.get("value2")[0]));
            calculation.setUnit1(Unit.valueOf(requestParameters.get("unit1")[0].toUpperCase()));
            if (operation.equals("add") || operation.equals("subtract")) {
                calculation.setUnit2(Unit.valueOf(requestParameters.get("unit2")[0].toUpperCase()));
            }

            if (requestParameters.containsKey("output_unit")) {
                var calculator = DistanceCalculatorFactory.getCalculator(Unit.valueOf(requestParameters.get("output_unit")[0]));
                calculation.setCalculator(calculator);
            } else {
                warningMessages.add("Missing parameter 'output_unit'. Defaulting to meters");
                calculation.setCalculator(DistanceCalculatorFactory.getCalculator(Unit.METER));
            }
        } catch (IllegalArgumentException e) {
            errorMessages.add("Invalid query parameters. Required parameters 'value1' and 'value2' should be decimal format; " +
                    "Required Parameters 'unit1' and 'unit2' allowed are 'METER', 'FT' or 'NM' as well as optional 'output_unit'; " +
                    "Required parameter 'operation' allowed is 'ADD', 'SUBTRACT', 'MULTIPLY', 'DIVIDE'.");
        }
        return calculation;
    }

    private List<String> validateParameters(Map<String, String[]> requestParameters, List<String> warningMessages) {
        final List<String> requiredParams = new ArrayList<>(Arrays.asList("value1", "value2", "unit1", "operation"));
        String operation;
        if (requestParameters.containsKey("operation")) {
            operation = requestParameters.get("operation")[0];
            if (operation.equals("add") || operation.equals("subtract")) {
                requiredParams.add("unit2");
            } else if (requestParameters.containsKey("unit2") && (operation.equals("divide") || operation.equals("multiply"))) {
                warningMessages.add("Divide and multiply operations don't support multiple input units, 'unit2' will be ignored");
            }
        }

        return requiredParams.stream()
                .filter(key -> !requestParameters.containsKey(key))
                .map(key -> String.format("Required parameter: %s  is missing.", key)).collect(Collectors.toList());

    }
}
