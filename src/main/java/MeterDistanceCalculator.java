public class MeterDistanceCalculator extends DistanceCalculator{
    @Override
    protected double getValueInRequestedUnit(Distance distance) {
        switch (distance.getUnit()){
            case METER:
                return distance.getValue();
            case FEET:
                return distance.getValue() * 0.3048;
            case NM:
                return distance.getValue() * 1852;
            default:
                throw new IllegalStateException("Unexpected value: " + distance.getUnit());
        }
    }

    @Override
    protected String getUnitValue() {
        return "m";
    }
}
