public class FeetDistanceCalculator extends DistanceCalculator{
    @Override
    protected double getValueInRequestedUnit(Distance distance) {
        switch (distance.getUnit()) {
            case METER:
                return distance.getValue() * 3.2808399;
            case FEET:
                return distance.getValue();
            case NM:
                return distance.getValue() * 6076.11549;
            default:
                throw new IllegalStateException("Unexpected value: " + distance.getUnit());
        }
    }

    @Override
    protected String getUnitValue() {
        return "ft";
    }
}
