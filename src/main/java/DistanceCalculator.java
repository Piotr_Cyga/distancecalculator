public abstract class DistanceCalculator {

    public double add(Distance a, Distance b){
        return getValueInRequestedUnit(a) + getValueInRequestedUnit(b);
    }

    public double subtract(Distance a, Distance b){
        var calculatedValueA = getValueInRequestedUnit(a);
        var calculatedValueB = getValueInRequestedUnit(b);

        if (calculatedValueA < calculatedValueB){
            throw new IllegalArgumentException("Result can't be negative. Verify input values.");
        }
        return  calculatedValueA - calculatedValueB;
    }

    public double multiply(Distance a, Distance b){
        return getValueInRequestedUnit(a) * b.getValue();
    }

    public double divide(Distance a, Distance b){
        if (b.getValue() == 0){
            throw new IllegalArgumentException("Can't divide by 0.");
        }
        return getValueInRequestedUnit(a) / b.getValue();

    }

    protected abstract double getValueInRequestedUnit(Distance distance);

    protected abstract String getUnitValue();
}
