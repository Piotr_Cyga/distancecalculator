public class NMDistanceCalculator extends DistanceCalculator{
    @Override
    protected double getValueInRequestedUnit(Distance distance) {
        switch (distance.getUnit()) {
            case METER:
                return distance.getValue() * 0.000539956803;
            case FEET:
                return distance.getValue() * 0.000164578834;
            case NM:
                return distance.getValue();
            default:
                throw new IllegalStateException("Unexpected value: " + distance.getUnit());
        }
    }

    @Override
    protected String getUnitValue() {
        return "NM";
    }
}
