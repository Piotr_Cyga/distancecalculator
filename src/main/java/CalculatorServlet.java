import org.json.JSONObject;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class CalculatorServlet extends HttpServlet {

    private static final DecimalFormat DF_2 = new DecimalFormat("#.##");
    private static final String ERRORS = "errors";

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        var requestParameters = request.getParameterMap();
        var warningMessages = new ArrayList<String>();
        var errorMessages = new ArrayList<String>();
        var jsonObject = new JSONObject();

        var calculation = CalculatorService.INSTANCE.createCalculation(requestParameters, errorMessages, warningMessages);

        if (!errorMessages.isEmpty()) {
            jsonObject.put(ERRORS, errorMessages);
            response.getOutputStream().println(jsonObject.toString());
            return;
        }

        var result = calculation.perform(errorMessages);

        if (!errorMessages.isEmpty()) {
            jsonObject.put(ERRORS, errorMessages);
            response.getOutputStream().println(jsonObject.toString());
            return;
        }

        jsonObject.put("value", DF_2.format(result));
        jsonObject.put("unit", calculation.getUnit());
        jsonObject.put("warningMessages", warningMessages);
        response.getOutputStream().println(jsonObject.toString());
    }
}
