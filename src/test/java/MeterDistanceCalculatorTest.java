import org.junit.Test;

import static org.junit.Assert.*;
public class MeterDistanceCalculatorTest {
    private DistanceCalculator calculator = new MeterDistanceCalculator();
    Distance nmDistance = new Distance(1.0, Unit.NM);
    Distance mDistance = new Distance(1.0, Unit.METER);
    Distance ftDistance = new Distance(1.0, Unit.FEET);
    Distance noUnit = new Distance(2.0, null);

    @Test
    public void testMeters(){
        assertEquals(2.0, calculator.add(mDistance, mDistance), 0);
        assertEquals(0.0, calculator.subtract(mDistance, mDistance), 0);
        assertEquals(0.5, calculator.divide(mDistance, noUnit), 0);
        assertEquals(2.0, calculator.multiply(mDistance, noUnit), 0);
    }

    @Test
    public void testNM(){
        assertEquals(3704.0, calculator.add(nmDistance, nmDistance), 0);
        assertEquals(0.0, calculator.subtract(nmDistance, nmDistance), 0);
        assertEquals(926.0, calculator.divide(nmDistance, noUnit), 0);
        assertEquals(3704.0, calculator.multiply(nmDistance, noUnit), 0);
    }

    @Test
    public void testFt(){
        assertEquals(0.6096, calculator.add(ftDistance, ftDistance), 0);
        assertEquals(0.0, calculator.subtract(ftDistance, ftDistance), 0);
        assertEquals(0.1524, calculator.divide(ftDistance, noUnit), 0);
        assertEquals(0.6096, calculator.multiply(ftDistance, noUnit), 0);
    }

    @Test
    public void testMixed(){
        assertEquals(1852.3048, calculator.add(ftDistance, nmDistance), 0);
        assertEquals(1853.0, calculator.add(mDistance, nmDistance), 0);
        assertEquals(1.3048, calculator.add(mDistance, ftDistance), 0);

        assertEquals(1851.6952, calculator.subtract(nmDistance, ftDistance), 0);
        assertEquals(1851.0, calculator.subtract(nmDistance, mDistance), 0);
        assertEquals(0.6952, calculator.subtract(mDistance, ftDistance), 0);
    }

    @Test
    public void subtractingLargerValue(){
        assertThrows(IllegalArgumentException.class, () -> calculator.subtract(ftDistance, nmDistance));
    }
}