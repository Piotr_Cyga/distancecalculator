import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class CalculatorServiceTest {

    private Map<String, String[]> requestParameters;
    private List<String> errorMessages;
    private List<String> warningMessages;

    @Test
    public void createCalculationPopulatesErrorsForMissingParameters() {
        requestParameters = new HashMap<>();
        errorMessages = new ArrayList<>();
        warningMessages = new ArrayList<>();

        CalculatorService.INSTANCE.createCalculation(requestParameters, errorMessages, warningMessages);

        assertEquals(4, errorMessages.size());
        assertTrue(errorMessages.stream().anyMatch(message -> message.contains("Required parameter: operation  is missing.")));
        assertTrue(errorMessages.stream().noneMatch(message -> message.contains("Required parameter: unit2  is missing.")));
    }

    @Test
    public void createCalculationPopulatesWarningForMissingUnit2WhenCalculationIsDivide() {
        requestParameters = new HashMap<>();
        requestParameters.put("operation", new String[]{"divide"});
        requestParameters.put("unit1", new String[]{"NM"});
        requestParameters.put("value1", new String[]{"1"});
        requestParameters.put("unit2", new String[]{"NM"});
        requestParameters.put("value2", new String[]{"2"});

        errorMessages = new ArrayList<>();
        warningMessages = new ArrayList<>();

        CalculatorService.INSTANCE.createCalculation(requestParameters, errorMessages, warningMessages);

        assertEquals(2, warningMessages.size());
        assertTrue(warningMessages.stream().anyMatch(message -> message.contains("'unit2' will be ignored")));
        assertTrue(warningMessages.stream().anyMatch(message -> message.contains(". Defaulting to meters")));
    }

    @Test
    public void createCalculationPopulatesErrorForIllegalValueOfAnyParameter() {
        requestParameters = new HashMap<>();
        requestParameters.put("operation", new String[]{"add"});
        requestParameters.put("unit1", new String[]{"mile"});
        requestParameters.put("value1", new String[]{"1"});
        requestParameters.put("unit2", new String[]{"NM"});
        requestParameters.put("value2", new String[]{"2"});

        errorMessages = new ArrayList<>();
        warningMessages = new ArrayList<>();

        CalculatorService.INSTANCE.createCalculation(requestParameters, errorMessages, warningMessages);

        assertEquals(1, errorMessages.size());
        assertTrue(errorMessages.stream().anyMatch(message -> message.contains("Invalid query parameters.")));
    }

    @Test
    public void createCalculationSuccessfulWhenAllParameters() {
        requestParameters = new HashMap<>();
        requestParameters.put("operation", new String[]{"add"});
        requestParameters.put("unit1", new String[]{"NM"});
        requestParameters.put("value1", new String[]{"1"});
        requestParameters.put("unit2", new String[]{"METER"});
        requestParameters.put("value2", new String[]{"2"});
        requestParameters.put("output_unit", new String[]{"NM"});

        errorMessages = new ArrayList<>();
        warningMessages = new ArrayList<>();

        var calculation = CalculatorService.INSTANCE.createCalculation(requestParameters, errorMessages, warningMessages);

        assertEquals(0, errorMessages.size());
        assertEquals(Unit.NM, calculation.getUnit1());
        assertEquals(Unit.METER, calculation.getUnit2());
        assertEquals(1, calculation.getValue1(), 0);
        assertEquals(2, calculation.getValue2(), 0);
        assertEquals("add", calculation.getOperation());
        assertTrue(calculation.getCalculator() instanceof NMDistanceCalculator);
    }
}