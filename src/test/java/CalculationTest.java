import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class CalculationTest {

    @Test
    public void calculationPopulatesErrorWhenResultNegative() {
        var errorMessages = new ArrayList<String>();
        var calculation = new Calculation(1.0, 2.0, Unit.NM, Unit.NM, "subtract", DistanceCalculatorFactory.getCalculator(Unit.NM));

        calculation.perform(errorMessages);

        assertTrue(errorMessages.contains("Result can't be negative. Verify input values."));
    }

    @Test
    public void calculationPopulatesErrorWhenDividingByZero() {
        var errorMessages = new ArrayList<String>();
        var calculation = new Calculation(1.0, 0, Unit.NM, null, "divide", DistanceCalculatorFactory.getCalculator(Unit.NM));

        calculation.perform(errorMessages);

        assertTrue(errorMessages.contains("Can't divide by 0."));
    }

    @Test
    public void calculationSuccessfulForValidInput() {
        var errorMessages = new ArrayList<String>();
        var calculation = new Calculation(3.0, 500.0, Unit.NM, Unit.METER, "subtract", DistanceCalculatorFactory.getCalculator(Unit.NM));

        assertEquals(2.73, calculation.perform(errorMessages), 0.01);
    }
}